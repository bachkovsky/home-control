{-# LANGUAGE OverloadedStrings #-}

module Main where

import ClassyPrelude

import qualified Clementine.Clementine as CLM
import qualified General.SystemControls as G
import Web.Scotty
import Data.Time

import Network.Wai.Middleware.RequestLogger (logStdout)

main :: IO ()
main =
  scotty 3000 $ do
    middleware logStdout
    clementineControls
    generalControls

clementineControls :: ScottyM ()
clementineControls = do
  get "/clementine/currentTrack" currentTrack
  post "/clementine/next" $ lift CLM.playNextTrack >> currentTrack
  post "/clementine/prev" $ lift CLM.playPreviousTrack >> currentTrack
  post "/clementine/volumeUp" (changeVolume CLM.volumeUp)
  post "/clementine/volumeDown" (changeVolume CLM.volumeDown)
  post "/clementine/playPause" $ lift CLM.playPause
  post "/clementine/like" $ lift CLM.like
  get "/clementine/randomTracks" $ do
    n <- param "n" `rescue` (\_ -> return 5)
    lift (CLM.randomTracks n) >>= json
  post "/clementine/playTrack" $ do
    tid <- param "trackId"
    lift $ CLM.playTrack (CLM.TrackId tid)
  get "/clementine/playlists" $
    lift CLM.allPlaylists >>= \res ->
      case res of
        CLM.Success pl -> json pl
        CLM.Failure err -> json (show err)
  post "/clementine/activatePlaylist" $ do
    playlistId <- param "plid"
    lift $ print playlistId
    lift $ CLM.activatePlaylist playlistId
  where
    changeVolume change = do
      delta <- param "delta" `rescue` (\_ -> return 5)
      lift $ change delta
    currentTrack = lift CLM.currentTrack >>= json

generalControls :: ScottyM ()
generalControls = do
  post "/general/volumeUp" $ lift G.volumeUp
  post "/general/volumeDown" $ lift G.volumeDown
  post "/general/lock" $ lift G.lock
  post "/general/suspend" $ lift G.suspend
  post "/general/lockAndSuspend" $ do
    time <- param "at" `rescue` (\_ -> return "")
    lift $ 
      if null time 
        then G.lockAndSuspend
        else G.lockAndSuspendAt (parseTimeOrError True defaultTimeLocale "%-d %b %Y, %H:%M" time)
