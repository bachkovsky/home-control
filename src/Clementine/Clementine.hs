{-# LANGUAGE OverloadedStrings #-}

module Clementine.Clementine
  ( TrackId(..)
  , TrackInfo(..)
  , playNextTrack
  , playPreviousTrack
  , currentTrack
  , playPause
  , volumeUp
  , volumeDown
  , like
  , randomTracks
  , playTrack
  , allPlaylists
  , Result(..)
  , activatePlaylist
  ) where

import ClassyPrelude

import Control.Arrow ((>>>))
import System.Process
import System.Random

import Clementine.Entity
import Clementine.PlaylistParser

import Data.Text (splitOn)
import Text.Read (read)

playNextTrack :: IO ()
playNextTrack =
  callCommand
    "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.Next"

playPreviousTrack :: IO ()
playPreviousTrack =
  callCommand
    "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.Prev"

playPause :: IO ()
playPause =
  callCommand
    "qdbus org.mpris.MediaPlayer2.clementine /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"

volumeUp :: Integer -> IO ()
volumeUp amount =
  callCommand $
  "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.VolumeUp " ++
  show amount

volumeDown :: Integer -> IO ()
volumeDown amount =
  callCommand $
  "qdbus org.mpris.MediaPlayer2.clementine /Player org.freedesktop.MediaPlayer.VolumeDown " ++
  show amount

like :: IO ()
like = callCommand "xdotool key super+Insert"

currentTrack :: IO TrackInfo
currentTrack =
  trackToTrackInfo <$> liftM2 Track currentTrackId currentTrackAttributes

currentTrackId :: IO TrackId
currentTrackId =
  TrackId . read <$>
  readProcess
    "qdbus"
    [ "org.mpris.clementine"
    , "/TrackList"
    , "org.freedesktop.MediaPlayer.GetCurrentTrack"
    ]
    ""

currentTrackAttributes :: IO (Map Text Text)
currentTrackAttributes =
  parseTrackAttributes . pack <$>
  readProcess
    "qdbus"
    [ "org.mpris.MediaPlayer2.clementine"
    , "/Player"
    , "org.freedesktop.MediaPlayer.GetMetadata"
    ]
    ""

playlistLength :: IO Integer
playlistLength =
  read <$>
  readProcess
    "qdbus"
    [ "org.mpris.clementine"
    , "/TrackList"
    , "org.freedesktop.MediaPlayer.GetLength"
    ]
    ""

randomTracks :: Integer -> IO [TrackInfo]
randomTracks n = do
  maxTracks <- playlistLength
  let numTracks = fromIntegral $ min n maxTracks
  trackIds <- replicateM numTracks (randomTrack maxTracks)
  maybeTracks <- traverse findTrack trackIds
  let tracks = catMaybes maybeTracks
  return $ trackToTrackInfo <$> tracks

randomTrack :: Integer -> IO TrackId
randomTrack maxId = TrackId <$> randomRIO (0, maxId)

findTrack :: TrackId -> IO (Maybe Track)
findTrack tid = (fmap . fmap) (Track tid) (findAttributes tid)
  where
    findAttributes (TrackId trackId') = do
      attrRaw <-
        readProcess
          "qdbus"
          [ "org.mpris.clementine"
          , "/TrackList"
          , "org.freedesktop.MediaPlayer.GetMetadata"
          , show trackId'
          ]
          ""
      let attr = parseTrackAttributes $ pack attrRaw
      return $
        if null attr
          then Nothing
          else Just attr

playTrack :: TrackId -> IO ()
playTrack (TrackId tid) =
  callCommand $
  "qdbus org.mpris.clementine /TrackList org.freedesktop.MediaPlayer.PlayTrack " ++
  show tid

parseTrackAttributes :: Text -> Map Text Text
parseTrackAttributes =
  splitOn "\n" >>>
  filter (not . null) >>> fmap (span (/= ':') >>> fmap (drop 2)) >>> mapFromList

allPlaylists :: IO (Result [Playlist])
allPlaylists = parsePlaylists <$> allPlaylistsRaw

playlistCount :: IO Int
playlistCount =
  read <$>
  readProcess
    "qdbus"
    [ "org.mpris.clementine"
    , "/org/mpris/MediaPlayer2"
    , "org.mpris.MediaPlayer2.Playlists.PlaylistCount"
    ]
    ""

allPlaylistsRaw :: IO Text
allPlaylistsRaw = playlistCount >>= playlistsRaw

playlistsRaw :: Int -> IO Text
playlistsRaw n =
  pack <$>
  readProcess
    "qdbus"
    [ "--literal"
    , "org.mpris.clementine"
    , "/org/mpris/MediaPlayer2"
    , "org.mpris.MediaPlayer2.Playlists.GetPlaylists"
    , "0"
    , show n
    , "0"
    , "0"
    ]
    ""

activatePlaylist :: Integer -> IO ()
activatePlaylist plid =
  callCommand $
  "qdbus org.mpris.clementine" <> " /org/mpris/MediaPlayer2" <>
  " org.mpris.MediaPlayer2.Playlists.ActivatePlaylist" <>
  " '/org/mpris/MediaPlayer2/Playlists/" <>
  show plid <>
  "'"
