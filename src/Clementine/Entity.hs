{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Clementine.Entity where

import ClassyPrelude

import Data.Aeson

data Track = Track
  { trackId :: TrackId
  , attributes :: Map Text Text
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

newtype TrackId =
  TrackId Integer
  deriving (Eq, Show, Generic, FromJSON, ToJSON)

data TrackInfo = TrackInfo
  { trackInfoId :: TrackId
  , artist :: Text
  , title :: Text
  , album :: Text
  , year :: Text
  , genre :: Text
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

trackToTrackInfo :: Track -> TrackInfo
trackToTrackInfo (Track tid attrs) =
  TrackInfo
    { trackInfoId = tid
    , artist = attr "artist"
    , title = attr "title"
    , album = attr "album"
    , year = attr "year"
    , genre = attr "genre"
    }
  where
    attr = flip (findWithDefault "Unknown") attrs

data Playlist = Playlist
  { playlistId :: Integer
  , playlistName :: Text
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)
