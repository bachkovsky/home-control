{-# LANGUAGE OverloadedStrings #-}

module Clementine.PlaylistParser
  ( parsePlaylists
  , Result(..)
  ) where

import ClassyPrelude

import Text.Parser.Combinators
import Text.Parser.Token
import Text.Trifecta

import Clementine.Entity (Playlist(..))

parsePlaylists :: Text -> Result [Playlist]
parsePlaylists = parseString playlists mempty . unpack

playlists :: Parser [Playlist]
playlists =
  brackets $ text "Argument: a(oss) " >> (braces $ sepBy playlist (text ", "))

playlist :: Parser Playlist
playlist =
  brackets $
  Playlist <$> playlistIdParser <* text ", " <*> playlistNameParser <*
  text ", \"\""

playlistIdParser :: Parser Integer
playlistIdParser =
  text "Argument: (oss) " >>
  text "[ObjectPath: /org/mpris/MediaPlayer2/Playlists/" *> decimal <* text "]"

playlistNameParser :: Parser Text
playlistNameParser = pack <$> stringLiteral
