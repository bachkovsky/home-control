module General.SystemControls
  ( volumeUp
  , volumeDown
  , lockAndSuspend
  , lockAndSuspendAt
  , lock
  , suspend
  ) where

import ClassyPrelude
import Data.Time
import System.Environment
import System.Process

volumeUp :: IO ()
volumeUp = changeVolume "+5%"

volumeDown :: IO ()
volumeDown = changeVolume "-5%"

lock :: IO ()
lock = getLockCmd >>= callCommand

getLockCmd :: IO String
getLockCmd = do
  home <- getEnv "HOME"
  return $ home ++ "/.local/bin/lock"

suspend :: IO ()
suspend = callCommand "systemctl suspend"

lockAndSuspend :: IO ()
lockAndSuspend = lock >> suspend

lockAndSuspendAt :: LocalTime -> IO ()
lockAndSuspendAt sleepAt = do
  now <- getCurrentTime
  tz <- getCurrentTimeZone
  let sleepAtUtc = localTimeToUTC tz sleepAt
  let (sleepSeconds, _) = properFraction $ diffUTCTime sleepAtUtc now
  delaySleep sleepSeconds

delaySleep :: Integer -> IO ()
delaySleep delay =
  when (delay > 0) $ do
    lockCmd <- getLockCmd
    spawnCommand $ "sleep " ++ (show delay) ++ "s && " ++ lockCmd ++ " && systemctl suspend"
    return ()

changeVolume :: String -> IO ()
changeVolume amount =
  callCommand $
  "pactl set-sink-volume $(pactl list sinks | grep RUNNING -B1 | grep -Po \'\\d+\') " ++
  amount ++ "; pkill -RTMIN+1 i3blocks"
